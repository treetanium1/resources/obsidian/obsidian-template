Table
```dataview
TABLE WITHOUT ID
  file.link AS "Name",
  ip as "IP#",
  (join(file.tags)) AS "Tags",
  user AS "User"
FROM "machines"
WHERE category
```

List
```dataview 
LIST rows.file.link
FROM "machines"
GROUP BY category
WHERE category
```

## Test machines
```dataview
TABLE WITHOUT ID
  file.link AS "Name",
  ip as "IP#",
  (join(file.tags)) AS "Tags",
  user AS "User"
FROM "machines"
WHERE contains(tags,"test")
```

## Servers
```dataview
TABLE WITHOUT ID
  file.link AS "Name",
  ip as "IP#",
  (join(file.tags)) AS "Tags",
  maintainer AS "Maintainer"
FROM "machines"
WHERE contains(category,"server")
```
