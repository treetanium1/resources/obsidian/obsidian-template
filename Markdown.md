---
url: https://daringfireball.net/projects/markdown/
---
## References
- [Cheat Markdown cheatsheet](file://.config/cheat/cheatsheets/personal/markdown)
- [Markdown Cheatsheet AdamP@MarkdownHere](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- https://www.markdownguide.org/
  - [Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
- Flavours
  - Kramdown: [Website](https://kramdown.gettalong.org/) | [Documentation](https://kramdown.gettalong.org/documentation.html) | [Quick reference](https://kramdown.gettalong.org/quickref.html) | [Github](https://github.com/gettalong/kramdown)
  - [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html)
  - CommonMark: used by [[Obsidian]]
- [[YAML]]


# Tools & Software
- [Grip](https://github.com/joeyespo/grip): Github flavour, requires browser and internet connection
- [[Obsidian]]
- [[software/Pandoc|Pandoc]]
- [[software/Software#^ec6fc0|glow]]

# Snippets
[Snippets folder](file://src)

## Tables
| Item | Price | # In stock |
|--------------|:-----:|-----------:|
| Juicy Apples | 1.99 | 739 |
| Bananas | 1.89 | 6 |
- use `|` for columns (and vertical lines), `-` for rows and horizontal lines
- use `:` for alignment

## Footnotes
Example [^1]
[^1]: Footnote text

## Code blocks
[_List of Markdown code block languages_](https://markdown.land/markdown-code-block)
![[list_markdown_codeblock_languages.txt]]

## Columns

Two fixed-width columns
<div style="display: flex; justify-content: space-between; width: 100%">
<div style="width: 45%">
Column 1
</div>
<div style="width: 45%">
Column 2
</div>
</div>

## Custom CSS

<!-- Doesn't work atm
Font Awesome icons: include relevant `.css` files, then use HTML
<i class="fa-brands fa-linux"></i> -->

# Graphs: Mermaid
[Website](https://mermaid.js.org) | [mermaid.live](https://mermaid.live/) | [My examples](file://rsrc/mermaid)
- Graph types
  - [Flowchart](https://mermaid.js.org/syntax/flowchart.html)
  - [Gantt](https://mermaid.js.org/syntax/gantt.html)
  - Git
  - [ENR (Entity Relationship)](https://mermaid.js.org/syntax/entityRelationshipDiagram.html#entity-relationship-diagrams)
  - [Mindmap (experimental)](https://mermaid.js.org/syntax/mindmap.html)
- [Theming](https://mermaid.js.org/config/theming.html)
- [[Pandoc]]: requires (lua) filter [[software/Pandoc#^959dc1|Pandoc Mermaid filter]]
- Tools
  - [![|20](github.png)mermaid-cli](https://github.com/mermaid-js/mermaid-cli): Command line tool for the Mermaid library
    ```bash
    mmdc -i input.md -o output.svg -t dark -b transparent
    ```

### Examples
Issues
- connecting a node in a subgraph breaks the subgraph's `direction`

Flow-chart: simple graph with *internal links*
```mermaid
graph LR

A[Biology]
B[Chemistry]

A --> B

class A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z internal-link;
```

Flow-chart: different node types, subgraphs, connections and a clickable external link
```mermaid
graph LR
a(a) --> b[b]
subgraph sg1[" "]
  direction TB
  c((c)) --> d[[d]]
  sg1a{<a>yay</a>} <--> sg1b{{nay}}
end
a -.-> sg1
sg1 --> e[(e)]

click sg1a "obsidian://open?file=Markdown"
class sg1a external-link;

dead1(("*")) --> id1>This is the text in the box]
```

Sequence
```mermaid
sequenceDiagram
    Alice->>+John: Hello John, how are you?
    Alice->>+John: John, can you hear me?
    John-->>-Alice: Hi Alice, I can hear you!
    John-->>-Alice: I feel great!
```

```mermaid
gitGraph
  commit
  commit id: "important fix"
```

# Presentations

## LaTeX Beamer

## reveal.js
See [[software/Software#Pandoc|Software: Pandoc]] and [[software/Software#reveal.js|Software: reveal.js]]
