---
title: Obsidian DataView
tags: Obsidian DataView
---
# `= this.title`
[Website](https://blacksmithgu.github.io/obsidian-dataview/) |  [Github](https://github.com/blacksmithgu/obsidian-dataview)
> Dataview is a live index and query engine over your personal knowledge base. You can [**add metadata**](https://blacksmithgu.github.io/obsidian-dataview/annotation/add-metadata/) to your notes and **query** them with the [**Dataview Query Language**](https://blacksmithgu.github.io/obsidian-dataview/queries/structure/) to list, filter, sort or group your data. Dataview keeps your queries always up to date and makes data aggregation a breeze.

- [Obsidian Example Vault for Dataview Queries](https://github.com/s-blu/obsidian_dataview_example_vault) | [Demo Website](https://s-blu.github.io/obsidian_dataview_example_vault/)
- [Omit "File" Column in TABLE results @ObsidianForum](https://github.com/blacksmithgu/obsidian-dataview/issues/102)
  ```
  TABLE WITHOUT ID
    col1, col2, ...
  ...
  ```

### Metadata
*Inline*
You can add data inline as [curry:: Thai]
or hiding the long key output in Reading mode (cake:: Bienenstich)

```markdown
I love `= this.curry` curry and `= this.cake` cake
```
I love `= this.curry` curry and `= this.cake` cake

### Queries
Types:
- `TABLE`
- `LIST`
- (`CALENDAR`)
- (`TASKS`)

Fields:
- simple metadata `myfield AS "Field"`
- use [functions](https://blacksmithgu.github.io/obsidian-dataview/reference/functions/) `("[[" + dateformat(date,"yyyy-MM-dd") + "]]") AS "Date"`
- tags: use `join` to merge tags array into one string and prevent each showing up on a separate line (in a table): `(join(file.tags)) AS "Tags"`
  Tags in this file: `=(join(this.tags))`

Note source: `FROM`
- tags: `FROM #tag`
- folders: `FROM "mydir"`
- incoming link: `FROM [[Note 1]]`
- outgoing link: `FROM outgoing([[Note1]])`
- exclude notes: `FROM "dir1" AND -"dir1/subdir"`

Filter metadata: `WHERE`
- partial match: `WHERE contains(field,"value)"`
- created within last dat: `WHERE file.ctime >= date(today) - dur(1 day)`

Sort results: `SORT`
- by modification date: `Sort file.mtime DESC`

Limit results: `LIMIT value`

[Example vault with queries](https://github.com/s-blu/obsidian_dataview_example_vault/)

### Functions
https://blacksmithgu.github.io/obsidian-dataview/reference/functions/
DataView uses [Luxon tokens](https://moment.github.io/luxon/#/formatting?id=table-of-tokens) for time formatting.
```
dateformat(field, "yyyy-MM-dd") # format dates
```


## Examples
[Dataview task and project examples](https://forum.obsidian.md/t/dataview-task-and-project-examples/17011)
[[Machines]]