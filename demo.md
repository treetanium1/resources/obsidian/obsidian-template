This is a little demo to showcase the features of Obsidian.

Obsidian mostly uses the CommonMark Markdown flavour. For a list of supported Markdown features, see [[Obsidian#Markdown]].

## Heading 2

- unsorted
- list

1. first step
2. second step

> What he said.

```bash
echo Hello
```

---

<hr>

<details><summary>Obsidian and HTML</summary>
supports most HTML tags as common Markdown viewers, some are sanitised
</details>

Obsidian adds
- Wiki-links: see [[Obsidian#^cecfe6|Obsidian: Wiki-links]] `[[...]]`
- comments by using `%%` %% like this %%
- call-outs
  > [!info]
  > Neat!
  
  > [!tip]- They can collapse
  > by adding a `-`
  

## Templates
- folder can be set in settings
- supports some variables
- accessible through quick commands: start typing `/`


## Plugins

### DataView
Parse [[Obsidian#Metadata, YAML Frontmatter|YAML frontmatter/Metadata]] to index and query notes to create lists, tables, ...