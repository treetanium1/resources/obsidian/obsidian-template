---
title: README Obsidian Template Repo
author: Sebastian Erfort
---
## Obsidian Template Vault

Repository with a few [[Markdown]] files, [[Obsidian]] example config and a demo of its [[features]]. Finally a demonstration of the Obsidian plugin [[DataView]].


## Usage

1. clone this repository

	```sh
	git clone <repo url>
	```

2. start Obsidian and open folder as vault
3. explore and edit


## References

- <https://obsidian.md>
- <https://blacksmithgu.github.io/obsidian-dataview/>

See also [my Obsidian notes online on (GitHub) Pages, using MkDocs](https://sebastianerfort.github.io/notes/).